
public class RunTemplate {

	public static void main(String[] args) {
		Soccer soccer = new Soccer();
		Chess chess = new Chess();
		
		chess.initialize();
		chess.start();
		chess.play();
		chess.end();
		
		System.out.println("<--------->");
		
		soccer.initialize();
		soccer.start();
		soccer.play();
		soccer.end();
		
		
		//output console
		
		/*Chess inicializado
		Press start...
		Playing chess
		Press exit...
		<--------->
		Soccer inicializado
		Starting soccer...
		Playing soccer...
		finishing Soccer...*/

	}

}
